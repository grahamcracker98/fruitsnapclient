import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FileUploadService } from '../file-upload.service';
import { Prediction } from '../../models/Prediction';

@Component({
  selector: 'app-capture',
  templateUrl: './capture.component.html',
  styleUrls: ['./capture.component.css']
})

export class CaptureComponent implements OnInit {

  file: File = null;
  prediction: string = null;

  constructor(private router: Router, private apiService: FileUploadService) { }

  ngOnInit() {
  }

  uploadFile() {
    this.apiService.getPrediction(this.file).subscribe((data: Prediction) => {
      this.prediction = data.prediction;
      this.router.navigate(['/results', this.prediction]).then(nav => {
        console.log(nav);
      },
      err => {
        console.log(err);
      })
    });
  }

  onImageChange(event) {
    const reader = new FileReader();
    const image = document.getElementById('myImage');

    reader.onload = function (e: any) {
      const src = document.createAttribute('src');
      src.value = e.target.result;
      image.attributes.setNamedItem(src);
    };
    reader.readAsDataURL(event.target.files[0]);
    this.file = event.target.files[0];
  }
}
