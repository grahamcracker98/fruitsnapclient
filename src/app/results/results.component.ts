import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  prediction: String = null;

  constructor(private route: ActivatedRoute,
    private router: Router) {
    this.prediction = this.route.snapshot.params['prediction'];
  }

  ngOnInit() {
    const image = document.getElementById('myImage');
    const src = document.createAttribute('src');
    console.log('assets/Samples/' + this.prediction + '.jpeg');
    src.value = 'assets/Samples/' + this.prediction + '.jpeg';
    image.attributes.setNamedItem(src);
  }

}
