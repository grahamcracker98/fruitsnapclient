import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatToolbarModule, MatSidenavModule, MatIconModule, MatListModule, MatCardModule, MatProgressSpinnerModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { MyNavComponent } from './my-nav/my-nav.component';
import { CaptureComponent } from './capture/capture.component';
import { AboutComponent } from './about/about.component';
import { NgxUploaderModule } from 'ngx-uploader';
import { ResultsComponent } from './results/results.component';
import { HttpClientModule } from '@angular/common/http';

const appRoutes: Routes = [
  { path: '', redirectTo: '/capture', pathMatch: 'full' },
  { path: 'capture', component: CaptureComponent, data: { title: 'Capture Component' } },
  { path: 'about', component: AboutComponent, data: { title: 'About Component' } },
  { path: 'results/:prediction', component: ResultsComponent, data: { title: 'Results Component' } }
];

@NgModule({
  declarations: [
    AppComponent,
    MyNavComponent,
    CaptureComponent,
    AboutComponent,
    ResultsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatListModule,
    NgxUploaderModule,
    RouterModule.forRoot(
      appRoutes,
      { useHash: true }
    ),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
